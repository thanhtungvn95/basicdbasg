CREATE DATABASE `university` ;
USE `university` ;

CREATE TABLE `university`.`STUDENTS` 
(
  `stno` INT NOT NULL,
  `name` VARCHAR(40) NULL,
  `addr` VARCHAR(40) NULL,
  `city` VARCHAR(40) NULL,
  `state` VARCHAR(4) NULL,
  `zip` VARCHAR(40) NULL
);

INSERT into STUDENTS VALUES (1011,'Edwards P. Davisd','10 Red Rd.','Newton','MA','02159');
INSERT into STUDENTS VALUES (2415,'Grogan A. Mary','8 Walnut St.','Malden','MA','02148');
INSERT into STUDENTS VALUES (2661,'Mixon Leatha','100 School St.','Brooklines','MA','02146');
INSERT into STUDENTS VALUES (2890,'McLane Sandy','30 Cass Rd.','Boston','MA','02122');
INSERT into STUDENTS VALUES (3442,'Novak Roland','42 Beacon St.','Nashua','NH','03060');
INSERT into STUDENTS VALUES (3566,'Pierce Richard','70 Park St.','Brooklines','MA','02146');
INSERT into STUDENTS VALUES (4022,'Prior Lorraine','8 Beacon St.','Boston','MA','02125');
INSERT into STUDENTS VALUES (5544,'Rawlings Jerry','15 Pleasant Dr.','Boston','MA','02115');
INSERT into STUDENTS VALUES (5571,'Lewis Jerry','1 Main Rd.','Providence','RI','02904');

CREATE TABLE `university`.`INSTRUCTORS` 
(
  `empno` INT NOT NULL,
  `name` VARCHAR(40) NULL,
  `rank` VARCHAR(40) NULL,
  `roomno` INT NULL,
  `telno` VARCHAR(40) NULL
);

INSERT into INSTRUCTORS VALUES (019,'Evans Robert','Professor',82,'7122');
INSERT into INSTRUCTORS VALUES (023,'Exxon George','Professor',90,'9101');
INSERT into INSTRUCTORS VALUES (056,'Sawyer Kathy','Assoc. Prof.',91,'5110');
INSERT into INSTRUCTORS VALUES (126,'Davis William','Asssoc. Prof',72,'5411');
INSERT into INSTRUCTORS VALUES (234,'Will Samuel','Assist. Prof.',90,'7024');

CREATE TABLE `university`.`COURSES` 
(
  `cno` VARCHAR(40) NOT NULL,
  `cname` VARCHAR(40) NULL,
  `cr` VARCHAR(40) NULL,
  `cap` VARCHAR(40) NULL
);

INSERT into COURSES VALUES ('cs110','Introduction to Computing','4','120');
INSERT into COURSES VALUES ('cs210','Computer Programming','4','100');
INSERT into COURSES VALUES ('cs240','Computer Archotecture','3','100');
INSERT into COURSES VALUES ('cs310','Data Structute','3','60');
INSERT into COURSES VALUES ('cs350','Higher Level Languages','3','50');
INSERT into COURSES VALUES ('cs410','Software Engineering','3','40');
INSERT into COURSES VALUES ('cs460','Graphics','3','30');

CREATE TABLE `university`.`GRADES` 
(
  `stno` INT NOT NULL,
  `empno` VARCHAR(40) NOT NULL,
  `cno` VARCHAR(40) NOT NULL,
  `sem` VARCHAR(40) NULL,
  `year` VARCHAR(40) NULL,
  `grade` VARCHAR(40) NULL
);

INSERT into GRADES VALUES (1011,'019','cs110','Fall','2001','40');
INSERT into GRADES VALUES (2661,'019','cs110','Fall','2001','80');
INSERT into GRADES VALUES (3566,'019','cs110','Fall','2001','95');
INSERT into GRADES VALUES (5544,'019','cs110','Fall','2001','100');
INSERT into GRADES VALUES (1011,'023','cs110','Spring','2002','75');
INSERT into GRADES VALUES (4022,'023','cs110','Spring','2002','60');
INSERT into GRADES VALUES (3566,'019','cs240','Spring','2002','100');
INSERT into GRADES VALUES (5571,'019','cs240','Spring','2002','50');
INSERT into GRADES VALUES (2415,'019','cs240','Spring','2002','100');
INSERT into GRADES VALUES (3442,'234','cs410','Spring','2002','60');
INSERT into GRADES VALUES (5571,'234','cs410','Spring','2002','80');
INSERT into GRADES VALUES (1011,'019','cs210','Fall','2002','90');
INSERT into GRADES VALUES (2661,'019','cs210','Fall','2002','70');
INSERT into GRADES VALUES (3566,'019','cs210','Fall','2002','90');
INSERT into GRADES VALUES (5571,'019','cs210','Spring','2003','85');
INSERT into GRADES VALUES (4022,'019','cs210','Spring','2003','70');
INSERT into GRADES VALUES (5544,'056','cs240','Spring','2003','70');
INSERT into GRADES VALUES (1011,'056','cs240','Spring','2003','90');
INSERT into GRADES VALUES (4022,'056','cs240','Spring','2003','80');
INSERT into GRADES VALUES (2661,'234','cs310','Spring','2003','100');
INSERT into GRADES VALUES (4022,'234','cs310','Spring','2003','75');

CREATE TABLE `university`.`ADVISING` 
(
  `stno` INT NOT NULL,
  `empno` VARCHAR(10) NOT NULL
 );
 
INSERT into ADVISING VALUES (1011,'019');
INSERT into ADVISING VALUES (2415,'019');
INSERT into ADVISING VALUES (2661,'023');
INSERT into ADVISING VALUES (2890,'023');
INSERT into ADVISING VALUES (3442,'056');
INSERT into ADVISING VALUES (3566,'126');
INSERT into ADVISING VALUES (4022,'234');
INSERT into ADVISING VALUES (5544,'023');
INSERT into ADVISING VALUES (5571,'234');



