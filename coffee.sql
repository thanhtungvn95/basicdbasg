CREATE DATABASE `coffee` ;
USE `coffee` ;

CREATE TABLE `coffee`.`CUSTOMER` 
(
  `cname` VARCHAR(40) NULL,
  `dob` VARCHAR(40) NULL,
  `cphoneno` INT NOT NULL,
  `add` VARCHAR(40) NULL,
  `voucher` VARCHAR(4) NULL
);

INSERT into CUSTOMER VALUES ('Nguyen Tri Dung','07/01/1995','01652153467','Ha Noi','10%');
INSERT into CUSTOMER VALUES ('Kieu Minh Khoa','24/05/1995','01668327046','Phan Thiet','10%');
INSERT into CUSTOMER VALUES ('Truong Tuan Ngoc','30/10/1995','01647733743','Ha Noi','10%');
INSERT into CUSTOMER VALUES ('Dang Nguyen Hai Thu','10/09/1995','01674083198','Ha Noi','10%');
INSERT into CUSTOMER VALUES ('Pham Quoc Trung','18/02/1995','0935579303','Da Nang','10%');
INSERT into CUSTOMER VALUES ('Nguyen Thanh Tung','06/08/1995','0919589679','Bac Ninh','10%');

CREATE TABLE `coffee`.`SELLER` 
(
  `sID` VARCHAR(40) NULL,
  `sName` VARCHAR(40) NULL,
  `sphoneno` INT NULL
);

INSERT into SELLER VALUES ('SE01','Nguyen Thu Hang',01642355463);
INSERT into SELLER VALUES ('SE02','Tran Thi Thuy',01657533846);
INSERT into SELLER VALUES ('SE03','Truong Ha My',01624327934);

CREATE TABLE `coffee`.`PRODUCT` 
(
  `pID` VARCHAR(40) NOT NULL,
  `pName` VARCHAR(40) NULL,
  `quantity` VARCHAR(40) NULL,
  `promotion` VARCHAR(40) NULL
);

INSERT into PRODUCT VALUES ('CF01','Iced Coffee','10','10%');
INSERT into PRODUCT VALUES ('CF02','Iced Espresso Classics - Vanilla Latte','15','10%');
INSERT into PRODUCT VALUES ('CH01','Chocolate Beverages','14','10%');
INSERT into PRODUCT VALUES ('FR01','Blonde Roast','20','10%');
INSERT into PRODUCT VALUES ('FR02','Featured Dark Roast','22','10%');
INSERT into PRODUCT VALUES ('CF03','Cold Brew Coffee','17','10%');
INSERT into PRODUCT VALUES ('TE01','Shaken Sweet Tea','21','10%');
INSERT into PRODUCT VALUES ('CH02','Chocolate Smoothie','16','10%');
INSERT into PRODUCT VALUES ('FR03','Orange Mango Smoothie','19','10%');
INSERT into PRODUCT VALUES ('FR04','Strawberry Smoothie','13','10%');


