Name: Nguyen Thanh Tung
ID: USTHBI4 - 159

1.
select stno,name,AVG(grade)
from (students natural join grades)
group by name;

2.
select stno
from (select stno, MAX(grade) as highest, MIN(grade) as lowest
from grades
group by stno) as X
where (highest-lowest) < 20;

3.
select cno, count(cno) as 'number of student', MIN(grade), MAX(grade), AVG(grade) 
from grades
group by cno;

4.
select stno, AVG(grade)
from (students natural join grades)
where cno='cs110'
group by stno;

5.
select empno, name, count(empno) as '# of student'
from (instructors natural join advising)
group by empno
order by count(empno) DESC
limit 3;

6.
select empno, name, count(distinct cno) as '# of course'
from (grades natural join instructors)
group by empno;

7.
select empno, name, count(distinct cno) as '# of course'
from (grades natural join instructors)
group by name
order by count(distinct cno) DESC;

8.
select empno, name, count(distinct cno) as '# of course'
from (grades natural join instructors)
group by name
order by count(distinct cno) DESC
limit 3;

9.
select distinct
o.empno, o.sem, d.numbercourse
from (select sem, count(distinct cno) as numbercourse
from grades
group by sem
having count(distinct cno) > 1
) as d inner join grades o on o.sem = d.sem
order by empno;